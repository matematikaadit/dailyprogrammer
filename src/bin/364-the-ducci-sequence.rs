// Run it by executing:
// ```bash
// $ cargo build --release --bin 364-the-ducci-sequence
// $ RUST_LOG=info target/release/364-the-ducci-sequence
// ```
#[macro_use] extern crate log;
extern crate env_logger;
const LIMIT: u32 = 1_000_000;

use std::collections::HashSet;

/// Next ducci sequence of the given tupple
fn ducci_next(xs: Vec<i32>) -> Vec<i32> {
    let n = xs.len();
    let mut ys = Vec::with_capacity(n);
    for i in 0..n {
        ys.push((xs[i] - xs[(i+1)%n]).abs());
    }
    ys
}

/// Number of steps until it reach stable sequence
fn ducci_steps(start: Vec<i32>) -> u32 {
    let mut memo = HashSet::new();
    let mut current = start;
    let mut steps = 0;
    while !memo.contains(&current) || steps > LIMIT {
        steps += 1;
        memo.insert(current.clone());

        info!("{:?}: {:?}", steps, current);
        current = ducci_next(current);
    }
    steps
}

fn main() {
    env_logger::Builder::from_default_env()
        .default_format_timestamp(false)
        .default_format_module_path(false)
        .init();

    let test_cases = vec![
        vec![0, 653, 1854, 4063],
        vec![1, 5, 7, 9, 9],
        vec![1, 2, 1, 2, 1, 0],
        vec![10, 12, 41, 62, 31, 50],
        vec![10, 12, 41, 62, 31],
    ];

    for tc in test_cases.into_iter() {
        println!("{:?}", tc);
        println!("{} steps", ducci_steps(tc));
    }
}
